// import logo from './logo.svg';
// import './App.css';

import { gCars, otoCuHayMoi } from "./info";

function App() {
  return (
    <div className="App">
      {/* <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header> */}
      <ul>
        {
          gCars.map((value,index) => {
            return <li>{value.make + ' ' + value.vID + " " + otoCuHayMoi(value.year)}</li>
          })
        }
      </ul>
    </div>
  );
}

export default App;
