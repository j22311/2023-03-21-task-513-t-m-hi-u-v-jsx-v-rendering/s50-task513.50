const gJsonCars = `[{"make":"Ford","model":"Explorer","year":2017,"color":"white","vID":"AB-10111"},
{"make":"Toyota","model":"Corolla","year":2018,"color":"silver","vID":"DN-23218"},
{"make":"Mazda","model":"Mazda 6","year":2020,"color":"red","vID":"TZ-23212"},
{"make":"Toyota","model":"Fortuna","year":2016,"color":"black","vID":"IN-91925"},
{"make":"Mazda","model":"Mazda 3","year":2019,"color":"silver","vID":"MN-44593"}]`;
const gCars = JSON.parse(gJsonCars);
// const otoCuHayMoi = gCars.map((value,index) => {
//   return value.year > 2018 ? "xe oto mới" : "xe oto cũ"
// })
function otoCuHayMoi(year){
  return year > 2018 ? "oto Cũ" : "oto Mới"
}

export {gCars, otoCuHayMoi}